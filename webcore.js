/**
 * @description Lightweight, feature-rich, all purpose library for the web.
 * @author ethancrist
 **/

// [[DEPENDENCIES]]
const name = 'Webcore'
const fs = require('fs')
const model = { }

// [[FUNCTIONS]]
const log = message => {
	// TODO: change to micro.log once you change iomicro
	console.log('['+name+'] ' + message)
}
const minify = code => {
	/**
	 * @purpose Minify JS or CSS code passed through as a String.
	 * @options #### code
	 * 	Type: `String` The prettified code passed as a String.
	 * @return `String` The minified JS and CSS code.
	 **/
   
	// TODO: Minify 'code' string
	return code
	//return code.replace(/(\r\n\t|\n|\r\t)/gm,'')
}
const cacheClient = () => { 
	/**
	 * @purpose Get all the Webcore client code as a long, minified string.
	 * @return `String` The JS and CSS code for Webcore.
	 **/
	log('Fetching and caching client...')

	// Resetting client cache...
	model.webcoreClient = ''

	// Looping through all files in ./client...
	const files = fs.readdirSync(model.clientDir)
	files.forEach(file => {
		// Ignoring hidden files in the ./client folder.
		if (file[0] === '.') return 

		// Reading contents of each file in ./client (Webcore CSS and JS)...
		const fileContent = fs.readFileSync(model.clientDir + file)

		log('Minifying client code '+file+'...')
		// Appending file contents to the "code" string variable...
		model.webcoreClient += minify(fileContent.toString())
	})

	return model.webcoreClient
}
const serve = (req, res, next) => {
	/**
	 * @purpose Express middleware run from code integration for Webcore.
	 **/

	// Redefining res.json so that the Webcore client will not pass through for res.json responses.
	res.json = (function(obj) {
		const oldResJson = res.json

		return function() {
			res.setHeader('Content-Type', 'application/json')

			oldResJson.apply(this, arguments)
		}
	})()

	// Redefining res.send into res.send with prepended Webcore CSS and JS code.
	// This is so that when the user calls res.send after implementing Webcore into their middleware, it will automagically prepend Webcore code onto every page with their model from res.send (this includes res.render since res.render uses res.send).
	res.send = (function(content) {
		const cachedFunction = res.send

		return function() {
			// NOT prepending the Webcore client code if:
            //      1) Response is a res.json response.
            //      2) dynamic=true was a query in the url i.e. 'site.org?dynamic=true'
			const sendClient = res.getHeader('Content-Type') !== 'application/json' && req.query.dynamic !== 'true'

			// arguments[0] is the content in res.send(content)
			// Here we prepend the webcore client code as a string to that content.
			if (sendClient) arguments[0] = model.webcoreClient+arguments[0]

			cachedFunction.apply(this, arguments)
		}
	})()

	next()
}
const Webcore = options => {
	/**
	 * @purpose Initialize Webcore server.
	 **/
	log('Initializing...')

	// NOTE ON CACHING:
		// The cache variable is a Unix timestamp when the server starts up.
		// This is appended to every file/resource loaded on the site, like: <fileName>?_=<cache>
		// This ensures that whenever a change is published and the server is restarted, all browsers will update their cache.
	model.cache = new Date().getTime()
	model.clientDir = __dirname+'/client/'

	// Storing the Webcore client cache into the model.
	cacheClient()

	log('Client minified and cached. '+name+' ready and serving.')
	return serve
}


// [[LANDING]]
module.exports = Webcore()
